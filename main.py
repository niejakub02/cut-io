import os
from flask import Flask, render_template, flash, request, redirect, url_for
from moviepy.editor import *

UPLOAD_FOLDER = 'static/input/'

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
ALLOWED_EXTENSIONS = {'mp4'}

def allowed_file(filename):
    return '.' in filename and \
            filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        video = request.files['video']

        if allowed_file(video.filename):
            video.save(os.path.join(app.config['UPLOAD_FOLDER'], video.filename))

    return redirect(url_for('edit', videoP = video.filename))

@app.route('/edit')
def edit():
    return render_template('edit.html', videoPath = request.args.get('videoP'))

@app.route('/cut', methods=['GET', 'POST'])
def hehe():
    if request.method == 'POST':
        start = request.form['start']
        end = request.form['end']
        filePath = request.form['filePath']

        clip = VideoFileClip("static/input/"+filePath)
        cut = clip.subclip(start,end)
        cut.write_videofile("static/output/cut.mp4")
    return redirect(url_for('index'))

if __name__ == "__main__":
    app.run(debug = True)