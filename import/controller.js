let currentX = 0;
let pressL = false;
let pressR = false;
let pressINS = false;
let leftMargin = 0;
let video = document.getElementById("edit-video");
let duration = (video.duration)/2;
let end = 0;
let onePix = 0;
let beginning = document.getElementById('beginning');
let ending = document.getElementById('ending');

window.addEventListener('load', function getOnePix() {
    document.getElementById("edit-video").onloadeddata = () =>
    {
        onePix = document.getElementById("edit-video").duration/400;
        beginning.innerHTML = video.seekable.start(0);
        ending.innerHTML = (video.seekable.end(0)/2).toFixed(1);
    }
});

window.onload = function() {
    onePix = document.getElementById("edit-video").duration/400;
    beginning.innerHTML = video.seekable.start(0);
    ending.innerHTML = (video.seekable.end(0)/2).toFixed(1);
  };

document.getElementById("right").addEventListener('mousedown', e =>
{
    e.preventDefault;
    currentX = e.clientX;
    pressR = true;
});

document.getElementById("left").addEventListener('mousedown', e =>
{
    e.preventDefault;
    currentX = e.clientX;
    pressL = true;
});

document.getElementById("inside-controller").addEventListener('mousedown', e =>
{
    e.preventDefault;
    currentX = e.clientX;
    pressINS = true;
});

document.addEventListener('mouseup', e => {
    e.preventDefault;
    pressL = false;
    pressR = false;
    pressINS = false;
});

document.addEventListener('mousemove', e => {
    e.preventDefault;
    changeDuration();
    if (pressR) {
        video.pause();
        let diff = e.clientX - currentX;
        if (diff != 0) {
            let widthR = document.getElementById('inside-controller').offsetWidth;
            widthR += diff;
            let max = 400 - leftMargin;

            document.getElementById('inside-controller').style.maxWidth = max+'px';
            document.getElementById('inside-controller').style.width = widthR+'px';
            document.getElementById('inside-controller').style.right = '0px';

            video.currentTime = ((leftMargin+document.getElementById('inside-controller').offsetWidth)*onePix).toFixed(1);

            beginning.innerHTML = (leftMargin*onePix).toFixed(1);
            ending.innerHTML = ((leftMargin+document.getElementById('inside-controller').offsetWidth)*onePix).toFixed(1);
        }
        currentX = e.clientX;
    }

    if (pressL || pressINS) {
        video.pause();
        let leftDiff = e.clientX - currentX;
            if(leftMargin + document.getElementById('inside-controller').offsetWidth > 400) { // blokowanie prawej krawędzi
                leftMargin -= 1;
            }
            else if (leftMargin < 0) { // blokowanie lewej krawędzi
                leftMargin = 0;
            }
            else { // ruch w obrębie kontrolera
                leftMargin += leftDiff;
            }

            if (leftMargin >= 0 && leftMargin <= 384 && document.getElementById('inside-controller').offsetWidth+leftMargin <= 400) {
                document.getElementById('inside-controller').style.left = leftMargin+'px';
                document.getElementById('inside-controller').style.right = '0px';
                
                if (!pressR) {
                changeDuration();
                changeStart();
                }

            beginning.innerHTML = (leftMargin*onePix).toFixed(1);
            ending.innerHTML = ((leftMargin+document.getElementById('inside-controller').offsetWidth)*onePix).toFixed(1);
        }
        currentX = e.clientX;
    }     
});

function changeStart() {
    video.duration = (video.duration).toFixed(1);
    video.currentTime = (leftMargin * onePix).toFixed(1); // ustawia lewy znacznik
}

function changeDuration() {
    duration = (document.getElementById('inside-controller').offsetWidth * onePix);
    duration.toFixed(1);
    end = leftMargin * onePix + duration;
}

function check() {
    if (video.paused == false) {
        if (video.currentTime >= (leftMargin*onePix) + duration) {
            video.pause();
        }
    }
}

function save() {
    let start = leftMargin*onePix;
    document.getElementById("start").value = start;
    document.getElementById("end").value = end;
    document.getElementById("submitForm").submit();
}

video.play = function() {
    let range = (document.getElementById('inside-controller').offsetWidth * onePix) + (leftMargin * onePix);

}

