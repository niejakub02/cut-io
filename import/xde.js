

$(document).ready(function() {
    $( "#dragThis" ).resizable({
        handles: "w,e",
        containment: "#container"
    });

    $( "#dragThis" ).draggable({
        containment: "#container",
        drag: function( event, ui ) {
            if (ui.position.left < 1) {
                ui.position.left = 0;
            }
        }
    });
  });