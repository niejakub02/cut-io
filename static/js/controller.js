var video = document.getElementById("edit-video");
var onePix = 0;

function setStartPoint() {
    video.currentTime = getStartPoint().toFixed(1);
}

function getStartPoint() {
    var barPosition = $(" #dragThis ").position();
    return startPoint = (barPosition.left * onePix);
}

function getDuration() {
    return duration = ($(" #dragThis ").width() * onePix);
}

function getEndPoint() {
    var x = parseFloat(getStartPoint());
    var y = parseFloat(getDuration());
    return x+y;
}

function setPointers() {
    $(" #beginning ").html(getStartPoint().toFixed(1));
    $(" #ending ").html(getEndPoint().toFixed(1));
}

$(document).ready(function() {
    video.onloadeddata = () => {
        onePix = video.duration/410;
        setPointers();
    }

    $( "#dragThis" ).resizable({
        handles: "w,e",
        containment: "#container",
        resize: function ( event,ui ) {
            setStartPoint();
            setPointers();
        }
    });

    $( "#dragThis" ).draggable({
        containment: "#container",
        drag: function( event, ui ) {
            if (ui.position.left < 1) {
                ui.position.left = 0;
            }
            setStartPoint();
            setPointers();
        }
    });

});

$(" button ").click(function() {
    $(" #start ").val(getStartPoint().toFixed(1));
    $(" #end ").val(getEndPoint().toFixed(1));
    $(" #submitForm ").submit();
});